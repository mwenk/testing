namespace Calculon.Core.Tests
{
  using Xunit;
  
  public class CalculatorTests
  {
    
    [Fact]
    public void Returns_Sum_When_2_Numbers_Are_Given()
    {
      // arrange
      var classUnderTests = new Calculator();
      
      // act
      var result = classUnderTests.Add(1, 1);
      
      // assert
      Assert.Equal(2, result);
    }
  }
}